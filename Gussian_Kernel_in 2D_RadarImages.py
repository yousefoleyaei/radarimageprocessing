#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  7 19:17:29 2017

@author: yousef
"""
"""
Created on Mon Jun 19 14:22:52 2017

@author: yousef
"""
import pylab
import numpy as np
from numpy.random import randn
import scipy
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
#import seaborn as sns
import scipy as sp
import glob
from scipy import ndimage, signal, stats
from PIL import Image
import time
import os, os.path
import matplotlib.pyplot as cm
import scipy.stats as st
import webbrowser
#import cv2
from scipy import misc
b=np.array([[227, 227, 227],        #Grey
                 [4, 233, 231],     #Bright Blue
                 [1, 154, 243],     #Medium Blue
                 [2, 1, 244],       #Dark Blue
                 [2, 250, 2],       #Bright Green
                 [1, 198, 1],       #Medium Green
                 [0, 142, 0],       #Dark Green
                 [253, 248, 2],     #Yellow
                 [238, 182, 1],     #Medium Yellow
                 [249, 146, 0],     #Orange
                 [253, 0, 0],       #Red
                 [212, 0, 0],       #Medium Red
                 [188, 0, 0],       #Dark Red
                 [248, 0, 253],     #Pink
                 [152, 84, 98],     #Purple
                 [254, 254, 254],   #White
                 [234, 234, 234]])  #Grey
def rgb2int(arr):
    """
    Convert (N,...M,3)-array of dtype uint8 to a (N,...,M)-array of dtype int32
    """
    return arr[...,0]*(256**2)+arr[...,1]*256+arr[...,2] #arr Red*(256**2)+arrGreen*256+arrBlue 

d=rgb2int(b)

Mapping2Threshold={d[0]:0,
              d[1]:5,
              d[2]:10,
              d[3]:15,
              d[4]:20,
              d[5]:25,
              d[6]:30,
              d[7]:35,
              d[8]:40,
              d[9]:45,
              d[10]:50,
              d[11]:55,
              d[12]:60,
              d[13]:65,
              d[14]:70,
              d[15]:75,
              d[16]:-10}

def g2dkern(kernlen, nsig):
    """
    Returns a 2D Gaussian kernel array.
    """
    interval = (2*nsig+1.)/(kernlen)
    x = np.linspace(-nsig-interval/2., nsig+interval/2., kernlen+1)
    kern1d = np.diff(st.norm.cdf(x))
    kernel_raw = np.sqrt(np.outer(kern1d, kern1d))
    kernel = kernel_raw/kernel_raw.sum()
    return kernel
#plt.imshow(g, interpolation='none')fil
DIR = '/home/yousef/Downloads/NEXTRADJUN27-17/'
i=0
for filename in sorted(glob.glob(DIR+'*.gif')): #assuming gif
    k=Image.open(filename).convert('RGB')
    d=os.path.basename(filename) # get the name of file in destination
    a = np.asarray(k)  
    m, n=k.size
    a=np.reshape(a,(m*n,3))
    c=rgb2int(a)
    c = sp.sparse.bsr_matrix([Mapping2Threshold.get(i,0) for i in c]).toarray()
    c=np.reshape(c,(n,m))
    g=np.array(g2dkern(21,3))
    z=signal.fftconvolve(c,g,mode='same')
    scipy.misc.imsave('Blurr_'+d, z)
#    scipy.misc.imshow(z)
    i+=1

